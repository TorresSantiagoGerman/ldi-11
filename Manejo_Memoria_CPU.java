/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacion_registros;

/**
 *
 * @author German
 */
public class Manejo_Memoria_CPU {
    
    public static void main(String []args)
    {
    //creamos un objeto que manejara la memoria
     CPU_MEMORIA mov= new CPU_MEMORIA();
   //el metodo .reg() ingresa una serie de 0 bits en los registro ax,bx,cx,dx.. es una especie de declaracion   
    mov.reg();
    //ingresamos una variable x
    mov.identificar("x", "byte", "10");
    //no ingresara la cantidad puesto que ya ha sido declarada
    mov.identificar("x", "byte", "100");
    // imprimimos la tabla de registros para saber que no ha sido remplazado
    mov.imp();
    //guardamos en dL el registro x
    mov.guardar("DL", "x");
    //almacenamos otra variable
    mov.identificar("y", "word", "131");
    //imprimimos tabla
    mov.imp();
    //guardamos Y en AL
    mov.guardar("AL", "y");
    //imprimimos la serie de bits almacenados
    mov.imprime();
     System.out.println();
    System.out.println("---------------------------");
    //ahora sumamos EL con AL... DONDE SE ALMACENARA ES EN EL
    mov.sumar_registro_registro("Dl", "al");
    //imprimimos la suma
    mov.imprime();
    
    //Nota: Esta valida la parte de identificadores... no puede ingresar un numero mayor que no sea byte 
    //asi mismo el limite del word
    //tengo dos metodos de sumar registros puesto que con uno sumo registro con un numero decimal y el otro sumo registros y variables
    //tengo dos metodos guardar de igual manera guardo decimales y tambien guardo registros :) 
    
    }
    
    
}
