package simulacion_registros;

import javax.swing.JTextArea;

/**
 * @author German
 */
public class CPU_MEMORIA {

    public String AX[], BX[], CX[], DX[];
    public int a[];
    public String CPU[][];
    final int FILAS = 10, COLUMNAS = 3;
    int i, j, mayor, menor;
    int filaMayor, filaMenor, colMayor, colMenor;
    String[][] A = new String[FILAS][COLUMNAS];
    int vari;
    public CPU_MEMORIA() {
        CPU = new String[3][100];
        AX = new String[16];
        BX = new String[16];
        CX = new String[16];
        DX = new String[16];
        a = new int[50];
        vari = 0;
    }
     public void reg() {
        for (int i = 0; i < AX.length; i++) {
            AX[i] = "0";
        }
        for (int i = 0; i < BX.length; i++) {
            BX[i] = "0";
        }
        for (int i = 0; i < CX.length; i++) {
            CX[i] = "0";
        }
        for (int i = 0; i < DX.length; i++) {
            DX[i] = "0";
        }
    }
    public void identificar(String variable, String tipo, String valor) {
        int val = Integer.parseInt(valor);
        if (tipo.equalsIgnoreCase("BYTE") && val <= 255 || tipo.equalsIgnoreCase("WORD") && val <= 65535) {
            boolean estado = false;
            if (vari == 0) {
                A[vari][0] = variable;
                A[vari][1] = tipo;
                A[vari][2] = valor;
                vari++;
            } else {
                for (int i = 0; i < vari; i++) {
                    if (A[i][0].equalsIgnoreCase(variable)) {
                        estado = true;
                    }
                }
                if (estado == false) {

                    A[vari][0] = variable;
                    A[vari][1] = tipo;
                    A[vari][2] = valor;
                    vari++;
                }
            }
        } else {
            System.out.println("Usted esta ingresando " + valor + " en un tipo de dato " + tipo);
        }
    }
    public void imp() {
        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                if (A[i][j] != null) {
                    System.out.print(A[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
    public String fin(String valor) {
        String var = "";
        for (int i = 0; i < vari; i++) {
            if (A[i][0].equalsIgnoreCase(valor)) {
                var = A[i][2];
            }
        }
        return var;
    }
    public String Suma_Binaria(String binario1, String binario2) {
        if (binario1 == null || binario2 == null) {
            return "";
        }
        int primer_elemento = binario1.length() - 1;
        int segundo_elemento = binario2.length() - 1;
        StringBuilder sb = new StringBuilder();
        int acarreado = 0;
        while (primer_elemento >= 0 || segundo_elemento >= 0) {
            int sum = acarreado;
            if (primer_elemento >= 0) {
                sum += binario1.charAt(primer_elemento) - '0';
                primer_elemento--;
            }
            if (segundo_elemento >= 0) {
                sum += binario2.charAt(segundo_elemento) - '0';
                segundo_elemento--;
            }
            acarreado = sum >> 1;
            sum = sum & 1;
            sb.append(sum == 0 ? '0' : '1');
        }
        if (acarreado > 0) {

            sb.append('1');
        }
        sb.reverse();
        return String.valueOf(sb);
    }
    public String conversion(int numero_natural) {
        JTextArea area = new JTextArea();
        int d, i, j;
        for (i = 0; numero_natural != 1; i++) {
            d = numero_natural % 2;
            a[i] = d;
            numero_natural = numero_natural / 2;
        }
        a[i] = 1;
        for (j = i; j >= 0; --j) {
            area.append("" + a[j]);
        }
        String n = area.getText();
        return n;
    }
    public void imprime() {
        for (int i = 0; i < AX.length; i++) {
            System.out.print("" + AX[i]);
        }
        System.out.println();
        for (int i = 0; i < BX.length; i++) {
            System.out.print("" + BX[i]);
        }
        System.out.println();
        for (int i = 0; i < CX.length; i++) {
            System.out.print("" + CX[i]);
        }
        System.out.println();
        for (int i = 0; i < DX.length; i++) {
            System.out.print("" + DX[i]);
        }
    }
    public void guardar(String registro, int decimal) {
        String convertido = conversion(decimal);
        if (registro.equalsIgnoreCase("AL") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = AX.length - 1; i >= AX.length - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AX") && decimal <= 65535) {
            int contador = convertido.length();
            for (int i = AX.length - 1; i >= (AX.length) - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = BX.length - 1; i >= BX.length - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal <= 65535) {
            int contador = convertido.length();
            for (int i = BX.length - 1; i >= (BX.length) - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = CX.length - 1; i >= CX.length - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal <= 65535) {
            int contador = convertido.length();
            for (int i = CX.length - 1; i >= (CX.length) - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = DX.length - 1; i >= DX.length - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal <= 255) {
            int contador = convertido.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal <= 65535) {
            int contador = convertido.length();
            for (int i = DX.length - 1; i >= (DX.length) - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
    }
    public String obtener_al() {
        StringBuilder area = new StringBuilder();
        for (int i = AX.length - 1; i >= AX.length - 8; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_ah() {
        StringBuilder area = new StringBuilder();
        for (int i = AX.length - 9; i >= 0; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_ax() {
        StringBuilder area = new StringBuilder();
        for (int i = AX.length - 1; i >= 0; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_bl() {
        StringBuilder area = new StringBuilder();
        for (int i = BX.length - 1; i >= BX.length - 8; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_bh() {
        StringBuilder area = new StringBuilder();
        for (int i = BX.length - 9; i >= 0; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";
      return a;
    }
    public String obtener_bx() {
        StringBuilder area = new StringBuilder();
        for (int i = BX.length - 1; i >= 0; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_cl() {
        StringBuilder area = new StringBuilder();
        for (int i = CX.length - 1; i >= CX.length - 8; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_ch() {
        StringBuilder area = new StringBuilder();
        for (int i = CX.length - 9; i >= 0; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_cx() {
        StringBuilder area = new StringBuilder();
        for (int i = CX.length - 1; i >= 0; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_dl() {
        StringBuilder area = new StringBuilder();
        for (int i = DX.length - 1; i >= DX.length - 8; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_dh() {
        StringBuilder area = new StringBuilder();
        for (int i = DX.length - 9; i >= 0; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public String obtener_dx() {
        StringBuilder area = new StringBuilder();
        for (int i = DX.length - 1; i >= 0; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";
        return a;
    }
    public void guardar(String registro, String decimal) {
        try {
            String valor = fin(decimal);
            String convertido = conversion(Integer.parseInt(valor));
            if (registro.equalsIgnoreCase("AL") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_al(), convertido);
                int contador = nuevo.length();
                for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                    AX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("AH") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_ah(), convertido);
                int contador = nuevo.length();
                for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                    AX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("AX") && Integer.parseInt(valor) <= 65535) {
                String nuevo = Suma_Binaria(obtener_ax(), convertido);
                int contador = nuevo.length();
                for (int i = AX.length - 1; i >= (AX.length) - nuevo.length(); i--) {
                    AX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("BL") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_bl(), convertido);
                int contador = nuevo.length();
                for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                    BX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("BH") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_bh(), convertido);
                int contador = nuevo.length();
                for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                    BX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("BX") && Integer.parseInt(valor) <= 65535) {
                String nuevo = Suma_Binaria(obtener_bx(), convertido);
                int contador = nuevo.length();
                for (int i = BX.length - 1; i >= (BX.length) - nuevo.length(); i--) {
                    BX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("CL") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_cl(), convertido);
                int contador = nuevo.length();
                for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                    CX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("CH") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_ch(), convertido);
                int contador = nuevo.length();
                for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                    CX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("CX") && Integer.parseInt(valor) <= 65535) {
                String nuevo = Suma_Binaria(obtener_cx(), convertido);
                int contador = nuevo.length();
                for (int i = CX.length - 1; i >= (CX.length) - nuevo.length(); i--) {
                    CX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("DL") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_dl(), convertido);
                int contador = nuevo.length();
                for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                    DX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("DH") && Integer.parseInt(valor) <= 255) {
                String nuevo = Suma_Binaria(obtener_dh(), convertido);
                int contador = nuevo.length();
                for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                    DX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
            if (registro.equalsIgnoreCase("DX") && Integer.parseInt(valor) <= 65535) {
                String nuevo = Suma_Binaria(obtener_dx(), convertido);
                int contador = nuevo.length();
                for (int i = DX.length - 1; i >= (DX.length) - nuevo.length(); i--) {
                    DX[i] = nuevo.charAt(contador - 1) + "";
                    contador--;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("No se encontro la variable solicitada");
        }
    }
    public void sumar_registro_decimal(String registro, int decimal) {
        String convertido = conversion(decimal);
        if (registro.equalsIgnoreCase("AL") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_al(), convertido);
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_ah(), convertido);
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AX") && decimal <= 65535) {
            String nuevo = Suma_Binaria(obtener_ax(), convertido);
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= (AX.length) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_bl(), convertido);
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_bh(), convertido);
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal <= 65535) {
            String nuevo = Suma_Binaria(obtener_bx(), convertido);
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= (BX.length) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_cl(), convertido);
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_ch(), convertido);
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal <= 65535) {
            String nuevo = Suma_Binaria(obtener_cx(), convertido);
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= (CX.length) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_dl(), convertido);
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal <= 255) {
            String nuevo = Suma_Binaria(obtener_dh(), convertido);
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal <= 65535) {
            String nuevo = Suma_Binaria(obtener_dx(), convertido);
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= (DX.length) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
    }
    public void sumar_registro_registro(String registro, String decimal) {
        if (registro.equalsIgnoreCase("AX") && decimal.equalsIgnoreCase("BX")) {
            String nuevo = Suma_Binaria(obtener_ax(), obtener_bx());
            int contador = nuevo.length();
            for (int p = AX.length - 1; p >= AX.length - nuevo.length(); p--) {
                AX[p] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AX") && decimal.equalsIgnoreCase("CX")) {
            String nuevo = Suma_Binaria(obtener_ax(), obtener_cx());
            int contador = nuevo.length();
            for (int b = AX.length - 1; b >= AX.length - nuevo.length(); b--) {
                AX[b] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AX") && decimal.equalsIgnoreCase("DX")) {
            String nuevo = Suma_Binaria(obtener_ax(), obtener_dx());
            int contador = nuevo.length();
            for (int c = AX.length - 1; c >= AX.length - nuevo.length(); c--) {
                AX[c] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal.equalsIgnoreCase("AX")) {
            String nuevo = Suma_Binaria(obtener_bx(), obtener_ax());
            int contador = nuevo.length();
            for (int aa = BX.length - 1; aa >= BX.length - nuevo.length(); aa--) {
                BX[aa] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal.equalsIgnoreCase("CX")) {
            String nuevo = Suma_Binaria(obtener_bx(), obtener_cx());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal.equalsIgnoreCase("DX")) {
            String nuevo = Suma_Binaria(obtener_bx(), obtener_dx());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal.equalsIgnoreCase("AX")) {
            String nuevo = Suma_Binaria(obtener_cx(), obtener_ax());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal.equalsIgnoreCase("BX")) {
            String nuevo = Suma_Binaria(obtener_cx(), obtener_bx());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal.equalsIgnoreCase("DX")) {
            String nuevo = Suma_Binaria(obtener_cx(), obtener_dx());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal.equalsIgnoreCase("AX")) {
            String nuevo = Suma_Binaria(obtener_dx(), obtener_ax());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal.equalsIgnoreCase("cX")) {
            String nuevo = Suma_Binaria(obtener_dx(), obtener_cx());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal.equalsIgnoreCase("BX")) {
            String nuevo = Suma_Binaria(obtener_dx(), obtener_bx());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AX") && decimal.equalsIgnoreCase("AX")) {
            String nuevo = Suma_Binaria(obtener_ax(), obtener_ax());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DX") && decimal.equalsIgnoreCase("DX")) {
            String nuevo = Suma_Binaria(obtener_dx(), obtener_dx());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BX") && decimal.equalsIgnoreCase("BX")) {
            String nuevo = Suma_Binaria(obtener_bx(), obtener_bx());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CX") && decimal.equalsIgnoreCase("CX")) {
            String nuevo = Suma_Binaria(obtener_cx(), obtener_cx());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("BH")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_bh());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("BL")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_bl());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("ch")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_ch());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("cl")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_cl());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("dH")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_dh());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("dl")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_dl());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("al")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_al());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal.equalsIgnoreCase("aH")) {
            String nuevo = Suma_Binaria(obtener_ah(), obtener_ah());
            int contador = nuevo.length();
            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("AH")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_ah());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_al());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("cH")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_ch());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("cl")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_cl());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("dH")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_dh());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("dl")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_dl());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("Bl")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_bl());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal.equalsIgnoreCase("Bh")) {
            String nuevo = Suma_Binaria(obtener_bh(), obtener_bh());
            int contador = nuevo.length();
            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("AH")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_ah());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_al());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("BH")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_bh());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("Bl")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_bl());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("DH")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_dh());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("dl")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_dl());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("Cl")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_cl());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal.equalsIgnoreCase("Ch")) {
            String nuevo = Suma_Binaria(obtener_ch(), obtener_ch());
            int contador = nuevo.length();
            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("AH")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_ah());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_al());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("cH")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_ch());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("cl")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_cl());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("dH") && decimal.equalsIgnoreCase("BH")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_bh());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("dH") && decimal.equalsIgnoreCase("bl")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_bl());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("DL")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_dl());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal.equalsIgnoreCase("Dh")) {
            String nuevo = Suma_Binaria(obtener_dh(), obtener_dh());
            int contador = nuevo.length();
            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("Bh")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_bh());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("Bl")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_bl());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("Ch")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_ch());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("CL")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_cl());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("Dh")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_dh());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("DL")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_dl());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AL") && decimal.equalsIgnoreCase("aL")) {
            String nuevo = Suma_Binaria(obtener_al(), obtener_al());
            int contador = nuevo.length();
            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("Ah")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_ah());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_al());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("Ch")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_ch());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("CL")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_cl());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("Dh")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_dh());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("DL")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_dl());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BL") && decimal.equalsIgnoreCase("bL")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_bl());
            int contador = nuevo.length();
            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("Ah")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_ah());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_al());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("Bh")) {
            String nuevo = Suma_Binaria(obtener_bl(), obtener_bh());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("BL")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_bl());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("Dh")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_dh());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("DL")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_dl());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CL") && decimal.equalsIgnoreCase("cL")) {
            String nuevo = Suma_Binaria(obtener_cl(), obtener_cl());
            int contador = nuevo.length();
            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("Ah")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_ah());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("AL")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_al());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("Ch")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_ch());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {

                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("CL")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_cl());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("Bh")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_bh());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("BL")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_bl());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DL") && decimal.equalsIgnoreCase("dL")) {
            String nuevo = Suma_Binaria(obtener_dl(), obtener_dl());
            int contador = nuevo.length();
            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
    }   
}